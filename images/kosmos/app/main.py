import uvicorn
import os
import io
from transformers import AutoProcessor, AutoModelForVision2Seq
from PIL import Image
from fastapi import FastAPI, UploadFile


app = FastAPI()


@app.on_event("startup")
def startup_event():
    app.model = AutoModelForVision2Seq.from_pretrained("microsoft/kosmos-2-patch14-224")
    app.processor = AutoProcessor.from_pretrained("microsoft/kosmos-2-patch14-224")


@app.post("/generate", name="Сгенерировать текст по изображению", tags=["AI Vision"])
async def caption(file: UploadFile, prompt="<grounding>An image of ", temp: float = 0.5, top_p: float = 0.5):
    image = Image.open(io.BytesIO(file.file.read())).convert('RGB')
    inputs = app.processor(text=prompt, images=image, return_tensors="pt")
    generated_ids = app.model.generate(pixel_values=inputs["pixel_values"],
                                       input_ids=inputs["input_ids"],
                                       attention_mask=inputs["attention_mask"],
                                       image_embeds=None,
                                       image_embeds_position_mask=inputs["image_embeds_position_mask"],
                                       max_new_tokens=512,
                                       do_sample=True,
                                       temperature=temp,
                                       top_k=0,
                                       top_p=top_p)
    generated_text = app.processor.batch_decode(generated_ids, skip_special_tokens=True)[0]
    processed_text, entities = app.processor.post_process_generation(generated_text)
    return processed_text


if __name__ == "__main__":
    config = uvicorn.Config("main:app",
                            port=80,
                            host="0.0.0.0",
                            log_level="info",
                            workers=os.cpu_count())
    server = uvicorn.Server(config)
    server.run()