<@requirement.NODE ref='kosmos' primary='kosmos-${namespace}' single='false' />

<@requirement.PARAM name='API_PORT' required='false' type='port' scope='global' />

<@img.TASK 'kosmos-${namespace}' 'imagenarium/kosmos:2.0-cpu'>
  <@img.NODE_REF 'kosmos' />
  <@img.VOLUME '/root/.cache' />
  <@img.PORT PARAMS.API_PORT '80' />
  <@img.CHECK_PORT '80' />
</@img.TASK>
